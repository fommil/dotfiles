(define-package "smithy-mode" "0.1.3" "Major mode for editing Smithy IDL files"
  '((emacs "26.1"))
  :commit "7dff0e7a497a055577226c7ae7ecdeaf7078b4c1" :authors
  '(("Matt Nemitz" . "matt.nemitz@gmail.com"))
  :maintainers
  '(("Matt Nemitz" . "matt.nemitz@gmail.com"))
  :maintainer
  '("Matt Nemitz" . "matt.nemitz@gmail.com")
  :keywords
  '("tools" "languages" "smithy" "idl" "amazon")
  :url "http://github.com/mnemitz/smithy-mode")
;; Local Variables:
;; no-byte-compile: t
;; End:
