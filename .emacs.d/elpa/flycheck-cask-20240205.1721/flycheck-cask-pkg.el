(define-package "flycheck-cask" "20240205.1721" "Cask support in Flycheck"
  '((emacs "24.3")
    (flycheck "0.14")
    (dash "2.4.0"))
  :commit "0eeec5197e9d31bfcfc39380b262d65259a87d91" :authors
  '(("Sebastian Wiesner" . "swiesner@lunaryorn.com"))
  :maintainers
  '(("Sebastian Wiesner" . "swiesner@lunaryorn.com"))
  :maintainer
  '("Sebastian Wiesner" . "swiesner@lunaryorn.com")
  :keywords
  '("tools" "convenience")
  :url "https://github.com/flycheck/flycheck-cask")
;; Local Variables:
;; no-byte-compile: t
;; End:
